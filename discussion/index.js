// Function able to receive data without theuse of global variables or prompt()


// "name" - is a parameter
// A Parameter is a variable or container that exists only in our function and is used to store information is provided to a function when it is called/invoked
function printName(name){
	console.log("My name is " + name);
};


// Data passed into a function invocation can be received by the function
// This is what we call an ARGUMENT
printName("Jungkook");
printName("Alexis");


// Data passed into the function through function invocation is called arguments
// The argument is then stored within a container is called a PARAMETER
function printMyAge(age){
	console.log("I am " + age)
};

printMyAge(23);
printMyAge();

// check divisibility reusably using a function with arguments and parameter
function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);

// MINI ACTIVITY

/*
	Mini-Activity

	1. Create a function which is capable to receive data as an argument:
		- This function should be able to receive the name of your favorite superhero
		- Display the name of your favorite superhero in the console
	 2. Create a function which is capable to receive a number as an argument:
	 	- This function should be able to receive a any number
	 	- Display the number and state if even number
 */

function printMyFavSuperhero(name){
	console.log("My favorite Superhero is " + name);
};

printMyFavSuperhero("Doctor Strange");

function checkDivisibilityBy2(num2){
	let evenNumber = num2 % 2 == 0;
	console.log("Is " + num2 + " an Even number?");
	console.log(evenNumber);
}

checkDivisibilityBy2(10);
checkDivisibilityBy2(15);
checkDivisibilityBy2(27);


// Multiple Arguments can also be passed into a function. Multiple parameters can contain our arguments

function printFullName(firstName, middleInitial, lastName){
	console.log(firstName + " " + middleInitial + " " + lastName);
};

printFullName("Juan", "Crisostomo", "Ibarra");
printFullName("Ibarra", "Juan", "Crisostomo");

/*
	Parameters will contain the argument according to the order it was passed

	In other language, providing more/less arguments than the expected parameters sometime causes an error or changes the behaviour of the function
*/

printFullName("Stephen", "Wardell");
printFullName("Stephen", "Wardell", "Curry", "Thomas");
printFullName("Stephen", " ", "Curry");


// Use variables as arguments

let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName, mName, lName);


// MINI ACTIVITY 2


function printFavSongs(song1, song2, song3, song4, song5){
console.log("My Top 5 Favorite Songs: ");
console.log("1. " + song1);
console.log("2. " + song2);
console.log("3. " + song3);
console.log("4. " + song4);
console.log("5. " + song5);

return song1 + song2 + song3 + song4 + song5;
// return "Hello, Sir Rupert!";

};

printFavSongs("Bohemian Rhapsody - Queen", "Make It Right (feat. Lauv) - BTS", "Happier Than Ever - Billie Eilish", "Getaway Car - Taylor Swift", "Ours - Taylor Swift");


// Return Statement
// Currently or so far, our function are able to display data in our console
// However, our function cannot yet return values. Function able to return values which can be saved into a variable using the return statement/keyword.

let fullName = printFullName("Gabrielle", "Vhernadette", "Fernandez");
console.log(fullName); // undefined

function returnFullName(firstName, middleInitial, lastName){
	return firstName + " " + middleInitial + " " + lastName;
};

fullName = returnFullName("Juan", "Ponce", "Enrile");
// printFullName(); return undefined because the function does not have return statement
// returnFullName(); return a string as a value because it has a return statement
console.log(fullName);

// Functions which have return statement are able to return value and it can be saved in a variable
console.log(fullName + " is my Granpa.");

function returnPhilippineAddress(city){
	return city + ", Philippines.";
};

// return -  return a values from a function whihc we can save in a variable
let myFullAddress = returnPhilippineAddress("San Pedro");
console.log(myFullAddress);

// returns true if number is divisible by 4, else returns false
function checkDivisibilityBy4(number){
	let remainder = number % 4;//remainder = 0
	let isDivisibleBy4 = remainder === 0;// true = 0 === 0
	// console.log(isDivisibleBy4);//true 
	console.log(remainder);

	// returns either true or false
	// Not only can you returnm raw values/data, you can also directly return a value
	// console.log(isDivisibleBy4);
	return remainder;//0
	// return isDivisibleBy4;//true
	// return keyword not only allows us to return value but also ends the process of the function
	console.log("I am run after the return.");
};

let num4isDivisibleBy4 = checkDivisibilityBy4(4);//true
// let num14isDivisibleBy4 = checkDivisibilityBy4(14);
// num4isDivisibleBy4();
checkDivisibilityBy4(4);
console.log(checkDivisibilityBy4(4));//0
console.log(num4isDivisibleBy4);//true; 0
// console.log(num14isDivisibleBy4);

function createPlayerInfo(username, level, job){
	// console.log('username: ' + username + ' level ' + level + ' Job: ' + job);
	return('username: ' + username + ' level ' + level + ' Job: ' + job);
	console.log(1+1);
};

let user1 = createPlayerInfo('white_night', 95, 'Paladin');
console.log(user1);

/*
	Mini-Activity
	Create a function which will be able to add two numbers
		- numbers must be provided as argument
		- display the result of the addition in our console
		- function should only display result. It should not return anything.

		- invoke and pass 2 arguments to the addition function
		- use return

 */


// MINI ACTIVITY 3

function answerAddition(sumAnswer1, sumAnswer2){
	let sumAll = sumAnswer1 + sumAnswer2;

	return sumAll; 
};

let sumOverall = answerAddition(10, 25);
console.log(sumOverall);

// MINI ACTIVITY 4

/*
	Create a function which will be able to multiply two numbers.
		- Numbers must be provided as arguments
		- Returm the result of multiplication

	Create a global variable called outside of the function called product
		- This product variable should be able to receive and store the result of multiplication function

	Log the value of product variable in the console

 */
function theProduct (prod1, prod2){
	return prod1 * prod2;

};

let product = theProduct (5,6);
console.log("The product is: ");
console.log(product)

// MINI ACTIVITY 5

function area1(radValue){
	return 3.14 * radValue**2;
	
}

let circleArea = area1(8);
console.log("The area is: ");
console.log(circleArea);


/* =====================
 Assignment:
 
 Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console. */

			console.log("Assignment!")
		function yourGrade (yourScore, totalScore){

			let theQuotient = (yourScore / totalScore)*100 ;
			let isPassed = theQuotient >= 75;

			console.log("The percentage of your grade is: " + theQuotient + "%");
			console.log("Your rating is: ");

			if (isPassed === true){
				return "Passed"
			} else {
				return "Failed"
			};

		};

		let isPassingScore =  yourGrade(40,50);
		console.log(isPassingScore);